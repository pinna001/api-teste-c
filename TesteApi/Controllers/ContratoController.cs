﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TesteApi.Data;
using TesteApi.Models;

namespace TesteApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContratoController : ControllerBase
    {
        private readonly IMemoryCache _cache;

        /// <summary>
        /// Lista os contratos registrados
        /// </summary>
        /// <returns>Objeto JSON contendo a lista de contratos registrados</returns> 
        
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Contrato>>> Get([FromServices] Context context)
        {
            List<Contrato> contratos;
            
            if (!_cache.TryGetValue("AllContratos", out contratos))
            {
                contratos = await context.Contratos
                .Include(x => x.prestacoes)
                .OrderBy(x => x.Id)
                .ToListAsync();

                _cache.Set("AllContratos", contratos, 
                    new MemoryCacheEntryOptions { AbsoluteExpiration = DateTime.Today });
            }
            
            if ( contratos != null)
            {
                contratos = setStatus(contratos);
            }            

            return contratos;
        }

        private List<Contrato> setStatus(List<Contrato> contratos)
        {
            var hoje = DateTime.Today;
            foreach (Contrato item in contratos)
            {
                if (item.prestacoes.Pagamento != null)
                {
                    item.prestacoes.Status = "Baixada";
                }
                else
                {
                    if (item.prestacoes.Vencimento >= hoje)
                    {
                        item.prestacoes.Status = "Aberta";
                    }
                    else if (item.prestacoes.Vencimento < hoje)
                    {
                        item.prestacoes.Status = "Atrasada";
                    }
                }
                
            }
            return contratos;
        }

        /// <summary>
        /// Busca contrato pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna um objeto contrato identificado pelo campo ID</returns>
        
        [HttpGet]
        [Route("item/{id:int}")]
        public async Task<ActionResult<List<Contrato>>> GetItem(int id, [FromServices] Context context)
        {
            List<Contrato> contrato;

            if (!_cache.TryGetValue("ContratoItemCache"+ id , out contrato))
            {
                contrato = await context.Contratos
                .Include(x => x.prestacoes)
                .Where(x => x.Id == id)
                .ToListAsync();

                _cache.Set("ContratoItemCache" + id, contrato, 
                    new MemoryCacheEntryOptions { 
                        AbsoluteExpiration = DateTime.Today});
            }

            if (contrato != null)
            {
                contrato = setStatus(contrato);
            }            

            return contrato;
        }

        /// <summary>
        /// Cadastro de contrato
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna o Objeto Contrato cadastrado.</returns>

        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Contrato>> Post([FromServices] Context context, [FromBody] Contrato model)
        {
            if (ModelState.IsValid)
            {
                model.Id = null;            
                context.Contratos.Add(model);
                await context.SaveChangesAsync();
                return model;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Edição de Contrato
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorno do Objeto Contrato editado.</returns>
        
        [HttpPost]
        [Route("edit")]
        public async Task<ActionResult<Contrato>> EditContrato([FromServices] Context context, [FromBody] Contrato model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    context.Contratos.Update(model);
                    await context.SaveChangesAsync();
                    return model;
                }
                catch
                {
                    return BadRequest();
                }                
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Exclusão de Contrato
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorno de mensagem confirmando exclusão</returns>
        
        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult<String>> deleteContrato(int id, [FromServices] Context context)
        {
            var contrato = await context.Contratos.FindAsync(id);

            if (contrato != null)
            {
                context.Contratos.Remove(contrato);
                await context.SaveChangesAsync();
                return "Contrato excluído com sucesso!";
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
