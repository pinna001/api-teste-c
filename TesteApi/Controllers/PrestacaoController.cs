﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TesteApi.Data;
using TesteApi.Models;

namespace TesteApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrestacaoController : ControllerBase
    {
        /// <summary>
        /// Cadastro de Prestações
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna o objeto Prestação cadastrado</returns>
        
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Prestacao>> Post([FromServices] Context context, [FromBody] Prestacao model)
        {
            if (ModelState.IsValid)
            {
                context.Prestacoes.Add(model);
                await context.SaveChangesAsync();
                return model;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Buscar Prestação pelo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorna objeto Prestação identificado pelo campo ID.</returns>
        
        [HttpPost]
        [Route("item/{id:int}")]
        public async Task<ActionResult<List<Prestacao>>> GetItem(int id, [FromServices] Context context)
        {
            var prestacoes = await context.Prestacoes
                .Where(x => x.Id == id)
                .ToListAsync();

            return prestacoes;
        }

        /// <summary>
        /// Edição de prestação
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorno de objeto Prestação editado</returns>
        
        [HttpPost]
        [Route("edit")]
        public async Task<ActionResult<Prestacao>> EditPrestacao([FromBody] Prestacao model, [FromServices] Context context)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    context.Prestacoes.Update(model);
                    await context.SaveChangesAsync();
                    return model;
                }
                catch
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        /// <summary>
        /// Exclusão de Prestação
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Retorno de mensagem confirmando exclusão.</returns>
        
        [HttpDelete]
        [Route("{id:int}")]
        public async Task<ActionResult<string>> DeletePrestacao(int id, [FromServices] Context context)
        {
            var prestacao = await context.Prestacoes.FindAsync(id);

            if (prestacao != null)
            {
                try
                {
                    context.Prestacoes.Remove(prestacao);
                    await context.SaveChangesAsync();
                    return "Prestação excluída com sucesso!";
                }
                catch
                {
                    return BadRequest();
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
