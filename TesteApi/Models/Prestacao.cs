﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TesteApi.Models
{
    public class Prestacao
    {
        [Key]
        [Display(Name = "Identificador: ")]
        public int Id { get; set; }
        
        [Display(Name = "Data de Vencimento: ")]
        public DateTime Vencimento { get; set; }

        [Display(Name = "Data de Pagamento: ")]
        public DateTime? Pagamento { get; set; }

        [Display(Name = "Valor: ")]
        public Decimal Valor { get; set; }

        [Display(Name = "Status: ")]
        public String Status { get; set; }

        [Required(ErrorMessage = "Campo Obrigatório")]
        [Range(1, int.MaxValue, ErrorMessage = "Contrato Inválido")]
        public int ContratoId { get; set; }
        public Contrato Contrato { get; set; }

    }
}
