﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TesteApi.Models
{
    public class Contrato
    {
        [Key]
        public int? Id { get; set; }

        [Display(Name = "Data do Contrato: ")]
        public DateTime Data { get; set; }

        [Display(Name = "Quantidade de Parcelas: ")]
        public int Qtd { get; set; }

        [Display(Name = "Valor Financiado: ")]
        public Decimal Valor { get; set; }

        public Prestacao prestacoes { get; set; }
    }
}
