﻿using Microsoft.EntityFrameworkCore;
using TesteApi.Models;

namespace TesteApi.Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options)
        {

        }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<Prestacao> Prestacoes { get; set; }
    }
}
