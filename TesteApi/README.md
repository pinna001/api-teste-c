# Teste de API IBM

## Criar uma API com 2 entidades:
    Entidade Contrato: id (key autoincrement), data, quantidade de parcelas, valor financiado.
    Entidade Presta��o: contrato, data vencimento, data pagamento, valor, status (Aberta, Baixada, Atrasada).
 
O Campo Status dever� ser calculado com base no campo data vencimento, data atual e data pagamento, n�o sendo armazenado no banco.
     - data vencimento >= data atual e n�o tem data pagamento = Aberta
     - data vencimento < data atual e n�o tem data pagamento = Atrasada
     - data pagamento = Baixada.
 
N�o � necess�rio utilizar um banco de dados persistente, pode ser utiliza o InMemoryDB, mas se preferir utilizar banco n�o tem problema.
 
## Pontos que ser�o avaliados:
   - [2] - Model com relacionamento. (https://www.youtube.com/watch?v=but7jqjopKM)
   - [2] - API padr�o REST (criar, listar, editar, excluir) (necess�rio criar apenas apenas para contrato, ap�s criar o contrato o sistema sistema dever� gerar alguns boletos podendo ter 
   - [3] - Organiza��o do c�digo, SOLID, clean code.. use e abuse de boas pr�ticas (https://github.com/thangchung/clean-code-dotnet) 
   - [2] - Colocar o Swagger na sua api. (https://medium.com/@renato.groffe/asp-net-core-swagger-documentando-apis-com-o-package-swashbuckle-aspnetcore-5eef480ba1c0)   
   - [1] - Tem um erro nessa API do video do Balta, veja que para criar a categoria ela permite ser enviado o ID, e caso j� exista o mesmo ID, ir� ocorrer erro. Corrija n�o permitindo que o usu�rio envie o campo ID na requisi��o que cria o contrato.
   - [3]- InMemoryCache, ao consultar um contrato o mesmo gera o cache que dever� valer at� as 00:00hs. e as pr�ximas requisi��es retorna diretamente do cache.
   - [2] - Feature Flags (https://docs.microsoft.com/en-us/azure/azure-app-configuration/use-feature-flags-dotnet-core) - Colocar um par�metro feature Flag para que possa ser ativado / desativado o cache.
   - [4] - Teste automatizado com xUnit.
   - [4] - Utilizar pattern DDD.