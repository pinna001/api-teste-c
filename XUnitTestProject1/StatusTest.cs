using Microsoft.AspNetCore.Razor.Language;
using System;
using TesteApi.Models;
using TesteApi.Controllers;
using Xunit;
using Microsoft.VisualStudio.TestPlatform.Utilities;

namespace XUnitTestProject1
{
    public class StatusTest
    {
        [Fact]
        public void VerificaStatus_Aberta()
        {
            var item = new Contrato
            {
                Id = 1,
                Data = new DateTime(2020, 08, 01),
                Qtd = 3,
                Valor = 150m,
                prestacoes = new Prestacao
                {
                    Vencimento = new DateTime(2020, 09, 10),
                    Pagamento = null,
                    Status = ""
                }
            };


            // Private function SetStatus()

            var hoje = DateTime.Today;

            if (item.prestacoes.Pagamento != null)
            {
                item.prestacoes.Status = "Baixada";
            }
            else
            {
                if (item.prestacoes.Vencimento >= hoje)
                {
                    item.prestacoes.Status = "Aberta";
                }
                else if (item.prestacoes.Vencimento < hoje)
                {
                    item.prestacoes.Status = "Atrasada";
                }
            }

            Assert.Equal("Aberta", item.prestacoes.Status);
        }

        [Fact]
        public void VerificaStatus_Atrasada()
        {
            var item = new Contrato
            {
                Id = 1,
                Data = new DateTime(2020, 08, 01),
                Qtd = 3,
                Valor = 150m,
                prestacoes = new Prestacao
                {
                    Vencimento = new DateTime(2020, 08, 30),
                    Pagamento = null,
                    Status = ""
                }
            };

            // Private function SetStatus()

            var hoje = DateTime.Today;

            if (item.prestacoes.Pagamento != null)
            {
                item.prestacoes.Status = "Baixada";
            }
            else
            {
                if (item.prestacoes.Vencimento >= hoje)
                {
                    item.prestacoes.Status = "Aberta";
                }
                else if (item.prestacoes.Vencimento < hoje)
                {
                    item.prestacoes.Status = "Atrasada";
                }
            }

            Assert.Equal("Atrasada", item.prestacoes.Status);
        }

        [Fact]
        public void VerificaStatus_Baixada()
        {
            var item = new Contrato
            {
                Id = 1,
                Data = new DateTime(2020, 08, 01),
                Qtd = 3,
                Valor = 150m,
                prestacoes = new Prestacao
                {
                    Vencimento = new DateTime(2020, 09, 10),
                    Pagamento = new DateTime(2020, 08, 30),
                    Status = ""
                }
            };

            // Private function SetStatus()

            var hoje = DateTime.Today;

            if (item.prestacoes.Pagamento != null)
            {
                item.prestacoes.Status = "Baixada";
            }
            else
            {
                if (item.prestacoes.Vencimento >= hoje)
                {
                    item.prestacoes.Status = "Aberta";
                }
                else if (item.prestacoes.Vencimento < hoje)
                {
                    item.prestacoes.Status = "Atrasada";
                }
            }

            Assert.Equal("Baixada", item.prestacoes.Status);
        }
    }
}
